import Vue from 'vue'
import App from './App.vue'
import router from './router'
// import Vant from 'vant'
// import 'vant/lib/index.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 引入全局变量文件
import global from '@/global/index.vue'
import '@/styles/index.scss'

// 引入echarts
import echarts from 'echarts'
import VueResource from 'vue-resource'
import * as dd from 'dingtalk-jsapi';
// import VConsole from 'vconsole';

// Vue.use(Vant)
Vue.use(ElementUI)
Vue.use(VueResource)
Vue.use(require('vue-wechat-title'))

// Vue.use(echarts)
Vue.config.productionTip = false
Vue.prototype.$echarts = echarts
Vue.prototype.GLOBAL = global
// Vue.prototype.$vconsole = new VConsole();
Vue.prototype.$dd = dd;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
/* 路由发生变化修改页面title */
router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title;
  }
  next()
})
