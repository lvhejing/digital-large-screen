import axios from 'axios'
// import { Toast } from 'mint-ui'

const http = axios.create({
  baseURL: window.SITE_CONFIG.apiURL, // api的base_url
  timeout: 10000, // 请求超时时间
  withCredentials: false // 表示跨域请求时是否需要使用凭证
  // transformRequest: data => qs.stringify(data) //
})
// request拦截器
http.interceptors.request.use(
  e => {
    e.params = e.params || {}
    e.headers = e.headers || {}
    // set 默认值
    return e
  },
  error => ({ status: 0, msg: error.message })
)
// respone拦截器
http.interceptors.response.use(
  response => {
    const resp = response.data
    if (response.status === 200) {
      return resp
    }
    return resp
  },
  error => {
    if (error.response.status) {
      switch (error.response.status) {
        // 404请求不存在
        case 404:
          // Toast({
          //   message: '网络请求不存在',
          //   duration: 1500,
          //   forbidClick: true
          // })
          break
        // 其他错误，直接抛出错误提示
        default:
          // Toast({
          //   message: error.response.data.message,
          //   duration: 1500,
          //   forbidClick: true
          // })
      }
    }
    return Promise.reject(error)
  }
)
export default http
