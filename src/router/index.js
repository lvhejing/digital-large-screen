
import Vue from 'vue'
import Router from 'vue-router'
// import NotFound from '@/views/NotFound'
// import Index from '@/views/index'
// import Index1 from '@/views/index1'
// import Index2 from '@/views/index2'
//import Index3 from '@/views/index3'
// import Index4 from '@/views/index4'
// import witIndex from '@/views/witIndex'

Vue.use(Router)
export const routerMap = [
    {
      path: "/",
      name: "index",
      // component: Index,
      component: () => import('@/views/index.vue'),
      meta: {
        keepAlive: true, //此组件需要被缓存
        isBack:false, //用于判断上一个页面是哪个
        title:'数据展示系统（1/4）'
      }
    },
    {
      path: "/index1",
      name: "index1",
      // component: Index1,
      component: () => import('@/views/index1.vue'),
      meta: {
        keepAlive: true, //此组件需要被缓存
        isBack:false, //用于判断上一个页面是哪个
        title:'数据展示系统（2/4）'
      }
    },
    {
      path: "/index2",
      name: "index2",
      // component: Index2,
      component: () => import('@/views/index2.vue'),
      meta: {
        keepAlive: true, //此组件需要被缓存
        isBack:false, //用于判断上一个页面是哪个
        title:'数据展示系统（3/4）'
      }
    },
    // {
    //   path: "/index3",
    //   name: "index3",
    //   component: Index3,
    //   meta: {
    //     keepAlive: true, //此组件需要被缓存
    //     isBack:false, //用于判断上一个页面是哪个
    //     title:'数据展示系统'
    //   }
    // },
    // 新增页面
    {
      path: "/index4",
      name: "index4",
      component: () => import('@/views/index4.vue'),
      meta: {
        keepAlive: true, //此组件需要被缓存
        isBack:false, //用于判断上一个页面是哪个
        title:'数据展示系统（4/4）'
      }
    },
    // 移动版本
    {
      path: "/mobile/index1",
      name: "mobileIndex1",
      component: () => import('@/views/mobile/index1.vue'),
      meta: {
        keepAlive: true, //此组件需要被缓存
        isBack:false, //用于判断上一个页面是哪个
        title:'数据展示系统（1/5）'
      }
    },
    {
      path: "/mobile/index2",
      name: "mobileIndex2",
      component: () => import('@/views/mobile/index2.vue'),
      meta: {
        keepAlive: true, //此组件需要被缓存
        isBack:false, //用于判断上一个页面是哪个
        title:'数据展示系统（2/5）'
      }
    },
    {
      path: "/mobile/index3",
      name: "mobileIndex3",
      component: () => import('@/views/mobile/index3.vue'),
      meta: {
        keepAlive: true, //此组件需要被缓存
        isBack:false, //用于判断上一个页面是哪个
        title:'数据展示系统（3/5）'
      }
    },
    {
      path: "/mobile/index4",
      name: "mobileIndex4",
      component: () => import('@/views/mobile/index4.vue'),
      meta: {
        keepAlive: true, //此组件需要被缓存
        isBack:false, //用于判断上一个页面是哪个
        title:'数据展示系统（4/5）'
      }
    },
    {
      path: "/mobile/index5",
      name: "mobileIndex5",
      component: () => import('@/views/mobile/index5.vue'),
      meta: {
        keepAlive: true, //此组件需要被缓存
        isBack:false, //用于判断上一个页面是哪个
        title:'数据展示系统（5/5）'
      }
    },
    {
      path: "/wit-index",
      name: "witIndex",
      component: () => import('@/views/witIndex'),
        meta: {
            title:'数据展示系统'
        }
    },
    // , {
    //     path: "/404",
    //     name: "NotFound",
    //     component: NotFound
    // },
    { path: '*', redirect: '/404' }
]
export default new Router({
    // * 未添加 mode: http://localhost:8080/#/login
    // * 添加 mode: http://localhost:8080/login
    // * ’hash’、’history’、’abstract’，前两者是浏览器环境下，最后一个是支持Js的非浏览器环境
    // mode: '',
    // * 不是所有浏览器都支持 history 模式，如果遇到不支持的时候，需要设置 fallback 为 true，它会自动帮我们转成哈希去处理
    // * 如果你设置成 false，在不支持的情况下，那么单应用就会变成多应用，你每次路由跳转都会去后端然后返回新的内容，所以一般都是设置成 ture 要它去自动处理就好了
    fallback: true,
    // * 未添加 base: 链接与（未添加 mode || 添加 mode）时无变化
    // * 添加 base: http://localhost:8080/base/login
    base: '',
    routes: routerMap
})
